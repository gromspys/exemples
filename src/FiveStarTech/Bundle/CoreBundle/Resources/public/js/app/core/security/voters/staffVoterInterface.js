define(
    [],
    function () {
        'use strict';

        function IStaffVoter () {}

        IStaffVoter.prototype.getEvaluatedStaff = function () {
            throw new Error('Method getEvaluatedStaff is not implemented.');
        };

        return IStaffVoter;
    }
);
