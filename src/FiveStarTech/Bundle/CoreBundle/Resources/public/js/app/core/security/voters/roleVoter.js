define(
    [
        'abstract-voter',
        'app-config',
        'app-model-profile'
    ],
    function (abstractVoter,
              settings,
              profile) {
        'use strict';

        var voter = {
            ATTRIBUTES: {
                ROLE_USER: 'ROLE_USER',
                ROLE_ADMIN: 'ROLE_ADMIN',
                ROLE_SUPER_ADMIN: 'ROLE_SUPER_ADMIN',
                ROLE_FS_SALES: 'ROLE_FS_SALES',
                ROLE_FS_ADMIN: 'ROLE_FS_ADMIN',
                ROLE_STAFF: 'ROLE_STAFF',
                ROLE_AUDITOR: 'ROLE_AUDITOR',
                ROLE_TEACHER: 'ROLE_TEACHER',
                ROLE_STUDENT: 'ROLE_STUDENT',
                ROLE_PURCHASER: 'ROLE_PURCHASER',
                ROLE_EVALUATOR: 'ROLE_EVALUATOR',
                ROLE_SECONDARY_EVALUATOR: 'ROLE_SECONDARY_EVALUATOR',
                ROLE_PRIMARY_EVALUATOR: 'ROLE_PRIMARY_EVALUATOR',
                ROLE_SUPER_TEACHER: 'ROLE_SUPER_TEACHER',
                ROLE_PRINCIPAL: 'ROLE_PRINCIPAL',
                ROLE_PRINCIPAL_ASSISTANT: 'ROLE_PRINCIPAL_ASSISTANT',
                ROLE_SUPERINTENDENT: 'ROLE_SUPERINTENDENT',
                ROLE_SUPERINTENDENT_ASSISTANT: 'ROLE_SUPERINTENDENT_ASSISTANT',
                ROLE_CUSTOMER_ADMIN: 'ROLE_CUSTOMER_ADMIN'
            },

            vote: function (attributes, object) {
                var self = this,
                    result = settings.security.voters.accessAbstain,
                    roles;

                for (var i = 0; i < attributes.length; i++) {
                    if (!this.supportAttribute(attributes[i])) {
                        continue;
                    }

                    result = settings.security.voters.accessDenied;
                    roles = profile.get('roles') || [];
                    for (var j = 0; j < roles.length; j++) {
                        if (attributes[i] == roles[j]) {
                            return settings.security.voters.accessGranted;
                        }
                    }
                }

                return result;
            },

            supportAttribute: function (attribute) {
                return attribute.indexOf('ROLE_') === 0;
            }
        };


        return _.extend(
            _.clone(abstractVoter),
            voter
        );
    }
);
