define(
    [
        'app-config',
        'abstract-voter',
        'app-model-profile',
        'owner-voter-i'
    ],
    function (settings,
              abstractVoter,
              profile,
              IOwnerVoter) {
        'use strict';

        var voter = {
            ATTRIBUTES: {
                IS_OWNER: 'IS_OWNER'
            },

            vote: function (attributes, object) {
                if (!object || !(object.implemented(IOwnerVoter))) {
                    return settings.security.voters.accessAbstain;
                }

                for (var i = 0; i < attributes.length; i++) {
                    if (!this.supportAttribute(attributes[i])) {
                        continue;
                    }

                    switch (attributes[i]) {
                        case 'IS_OWNER':
                            return profile.id === this.getOwnerId(object) ?
                                settings.security.voters.accessGranted :
                                settings.security.voters.accessDenied;
                    }
                }

                return settings.security.voters.accessAbstain;
            },

            getOwnerId: function (object) {
                return object.getObjectOwner() ?
                    object.getObjectOwner().id :
                    NaN;
            }
        };

        return _.extend(
            _.clone(abstractVoter),
            voter
        );
    }
);
