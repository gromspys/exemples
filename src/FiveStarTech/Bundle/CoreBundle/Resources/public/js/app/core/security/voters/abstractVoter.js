define(
    [],
    function () {
        'use strict';

        var voter = {
            ATTRIBUTES: {},

            vote: function (attributes, object) {
                throw new Error('This function must be implemented.');
            },

            supportAttribute: function (attribute) {
                for (var key in this.ATTRIBUTES) {
                    if (attribute === this.ATTRIBUTES[key]) {
                        return true;
                    }
                }

                return false;
            }
        };

        return voter;
    }
);
