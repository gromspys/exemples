define(
    [
        'app-config',
        'role-voter',
        'readOnly-voter',
        'disable-module-voter',
        'staff-voter',
        'owner-voter',
        'subscriprionConfiguration-voter'
    ],
    function (
        settings,
        roleVoter,
        readOnlyVoter,
        disableModuleVoter,
        staffVoter,
        ownerVoter,
        subscriptionConfigurationVoter
    ) {
        'use strict';

        var accessManager = {
            ATTRIBUTES: null,
            ROLES: {
                USER: 'ROLE_USER',
                ADMIN: 'ROLE_ADMIN',
                SUPER_ADMIN: 'ROLE_SUPER_ADMIN',
                FS_SALES: 'ROLE_FS_SALES',
                FS_ADMIN: 'ROLE_FS_ADMIN',
                STAFF: 'ROLE_STAFF',
                AUDITOR: 'ROLE_AUDITOR',
                TEACHER: 'ROLE_TEACHER',
                STUDENT: 'ROLE_STUDENT',
                PURCHASER: 'ROLE_PURCHASER',
                EVALUATOR: 'ROLE_EVALUATOR',
                SECONDARY_EVALUATOR: 'ROLE_SECONDARY_EVALUATOR',
                PRIMARY_EVALUATOR: 'ROLE_PRIMARY_EVALUATOR',
                SUPER_TEACHER: 'ROLE_SUPER_TEACHER',
                PRINCIPAL: 'ROLE_PRINCIPAL',
                PRINCIPAL_ASSISTANT: 'ROLE_PRINCIPAL_ASSISTANT',
                SUPERINTENDENT: 'ROLE_SUPERINTENDENT',
                SUPERINTENDENT_ASSISTANT: 'ROLE_SUPERINTENDENT_ASSISTANT',
                CUSTOMER_ADMIN: 'ROLE_CUSTOMER_ADMIN'
            },
            voters: [
                roleVoter,
                readOnlyVoter,
                disableModuleVoter,
                staffVoter,
                ownerVoter,
                subscriptionConfigurationVoter
            ],
            isGranted: function (attributes, object) {
                for (var i = 0; i < this.voters.length; i++) {
                    switch (this.voters[i].vote(attributes, object)) {
                        case settings.security.voters.accessGranted:
                            return true;
                        case settings.security.voters.accessDenied:
                            return false;
                    }
                }

                return false;
            }
        };

        function extendAttributes() {
            var resultAttributes = {},
                voterAttributes,
                voters = accessManager.voters;

            for (var i in voters) {
                voterAttributes = voters[i].ATTRIBUTES;
                for (var key in voterAttributes) {
                    if (voterAttributes.hasOwnProperty(key) && resultAttributes.hasOwnProperty(key)) {
                        throw new Error('resultObj already has attribute ' + key);
                    }
                    if (key !== voterAttributes[key]) {
                        throw new Error(key + ' !== ' + voterAttributes[key]);
                    }
                }
                resultAttributes = _.extend(
                    resultAttributes,
                    voterAttributes
                )
            }

            return resultAttributes;
        }

        accessManager.ATTRIBUTES = extendAttributes();

        window.accessManager = accessManager;

        return accessManager;
    }
);
