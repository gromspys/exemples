define(
    [
        'abstract-voter',
        'app-config',
        'app-model-profile'
    ],
    function (abstractVoter,
              settings,
              profile) {
        'use strict';

        var voter = {
            ATTRIBUTES: {
                READ_ONLY: 'READ_ONLY'
            },

            vote: function (attributes, object) {
                for (var i = 0; i < attributes.length; i++) {
                    if (!this.supportAttribute(attributes[i])) {
                        continue;
                    }

                    if (profile.get('read_only')) {
                        return settings.security.voters.accessGranted;
                    }

                    return settings.security.voters.accessDenied;
                }

                return settings.security.voters.accessAbstain;
            }
        };

        return _.extend(
            _.clone(abstractVoter),
            voter
        );
    }
);
