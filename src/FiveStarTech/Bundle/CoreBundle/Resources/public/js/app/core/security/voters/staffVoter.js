define(
    [
        'abstract-voter',
        'app-config',
        'app-model-profile',
        'staff-voter-i'
    ],
    function (abstractVoter,
              settings,
              profile,
              IStaffVoter) {
        'use strict';

        var voter = {
            ATTRIBUTES: {
                STAFF_IS_EVALUATED_PERSON: 'STAFF_IS_EVALUATED_PERSON'
            },

            vote: function (attributes, object) {
                var profileStaffId;

                if (!object || !(object.implemented(IStaffVoter))) {
                    return settings.security.voters.accessAbstain;
                }

                profileStaffId = profile.get('staff') ? profile.get('staff').id : NaN;
                for (var i = 0; i < attributes.length; i++) {
                    if (!this.supportAttribute(attributes[i])) {
                        continue;
                    }

                    switch (attributes[i]) {
                        case 'STAFF_IS_EVALUATED_PERSON':
                            return profileStaffId === this.getStaffId(object) ?
                                settings.security.voters.accessGranted :
                                settings.security.voters.accessDenied;
                    }
                }

                return settings.security.voters.accessAbstain;
            },

            getStaffId: function (object) {
                return object.getEvaluatedStaff() ?
                    object.getEvaluatedStaff().id :
                    NaN;
            }
        };

        return _.extend(
            _.clone(abstractVoter),
            voter
        );
    }
);
