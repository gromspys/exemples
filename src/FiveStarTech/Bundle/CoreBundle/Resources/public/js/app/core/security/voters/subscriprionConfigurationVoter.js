define(
    [
        'abstract-voter',
        'app-config',
        'app-model-subscriptionConfiguration'
    ],
    function (abstractVoter,
              settings,
              subscriptionConfiguration) {
        'use strict';

        var voter = {
            ATTRIBUTES: {
                ALLOW_SE_FILES: 'ALLOW_SE_FILES'
            },

            vote: function (attributes, object) {
                for (var i = 0; i < attributes.length; i++) {
                    if (!this.supportAttribute(attributes[i])) {
                        continue;
                    }

                    switch (attributes[i]) {
                        case 'ALLOW_SE_FILES':
                            return subscriptionConfiguration.get('allow_staff_evaluations_files') ?
                                settings.security.voters.accessGranted :
                                settings.security.voters.accessDenied;
                    }
                }

                return settings.security.voters.accessAbstain;
            }
        };

        return _.extend(
            _.clone(abstractVoter),
            voter
        );
    }
);
