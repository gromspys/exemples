define(
    [
        'abstract-voter',
        'app-config',
        'app-model-subscriptionConfiguration'
    ],
    function (
        abstractVoter,
        settings,
        subscriptionConfiguration) {
        'use strict';

        var voter = {
            ATTRIBUTES: {
                ALLOW_STAFF_EVALUATIONS: 'ALLOW_STAFF_EVALUATIONS',
                ALLOW_DATA_WAREHOUSE: 'ALLOW_DATA_WAREHOUSE',
                ALLOW_DAILY_ASSESSMENTS: 'ALLOW_DAILY_ASSESSMENTS',
                ALLOW_CURRICULUM_MAPPING: 'ALLOW_CURRICULUM_MAPPING',
                ALLOW_RESPONSE_TO_INTERVENTIONS: 'ALLOW_RESPONSE_TO_INTERVENTIONS'
            },

            vote: function (attributes, object) {
                var self = this,
                    result = settings.security.voters.accessAbstain,
                    enableModule;

                for (var i = 0; i < attributes.length; i++) {
                    if (!self.supportAttribute(attributes[i])) {
                        continue;
                    }

                    result = settings.security.voters.accessDenied;

                    switch (attributes[i]) {
                        case 'ALLOW_STAFF_EVALUATIONS':
                            enableModule = subscriptionConfiguration.get('allowed_modules').SE;
                            break;
                        case 'ALLOW_DATA_WAREHOUSE':
                            enableModule = subscriptionConfiguration.get('allowed_modules').DW;
                            break;
                        case 'ALLOW_DAILY_ASSESSMENTS':
                            enableModule = subscriptionConfiguration.get('allowed_modules').DA;
                            break;
                        case 'ALLOW_CURRICULUM_MAPPING':
                            enableModule = subscriptionConfiguration.get('allowed_modules').CM;
                            break;
                        case 'ALLOW_RESPONSE_TO_INTERVENTIONS':
                            enableModule = subscriptionConfiguration.get('allowed_modules').RTI;
                            break;
                    }

                    if (enableModule) {
                        return settings.security.voters.accessGranted;
                    }
                }

                return result;
            }
        };

        return _.extend(
            _.clone(abstractVoter),
            voter
        );
    }
);
