define(
    [],
    function () {
        'use strict';

        function IOwnerVoter () {}

        IOwnerVoter.prototype.getObjectOwner = function () {
            throw new Error('Method getObjectOwner is not implemented.');
        };

        return IOwnerVoter;
    }
);
