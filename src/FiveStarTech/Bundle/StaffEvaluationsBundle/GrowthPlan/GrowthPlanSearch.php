<?php
/*
 * Copyright © Five-Star Technology Solutions
 *
 * For a full copyright notice, see the LICENSE file.
*/

namespace FiveStarTech\Bundle\StaffEvaluationsBundle\GrowthPlan;

use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\Query\Expr\Composite;
use Doctrine\ORM\QueryBuilder;
use FiveStarTech\Bundle\CoreBundle\Search\PaginationTrait;
use FiveStarTech\Bundle\CoreBundle\Status;
use FiveStarTech\Bundle\StaffEvaluationsBundle\EvaluatedStaff\Model\Types\EvaluatedStaffTypes;
use FiveStarTech\Bundle\UserBundle\User\Exception\NonAuthenticationUserException;
use FiveStarTech\Bundle\UserBundle\User\Handler\UserInvoker;
use FiveStarTech\Bundle\UserBundle\User\Role;
use FiveStarTech\Bundle\UserBundle\User\StaffRepository;
use FiveStarTech\Core\Exception\NotFoundException;
use FiveStarTech\Core\Model\Search;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Sergey Korneev <sergey.korneev@opensoftdev.ru>
 */
final class GrowthPlanSearch implements Search
{
    use PaginationTrait;

    /**
     * @var int
     */
    public $staffId = 0;

    /**
     * @var array
     */
    public $statuses = array();

    /**
     * @var int
     */
    private $currentUserId;

    /**
     * @var int
     */
    private $currentStaffId;

    /**
     * @var bool
     */
    private $findForAdmin = false;

    /**
     * @var bool
     */
    private $findForEvaluator = false;

    /**
     * @param UserInvoker $userInvoker
     */
    public function __construct(UserInvoker $userInvoker)
    {
        try {
            $this->currentUserId = $userInvoker->getCurrentUser()->getId();
            $this->currentStaffId = $userInvoker->getCurrentStaff()->getId();

            $context = $userInvoker->getSecurityContext();

            $this->findForAdmin = $context->isGranted(Role::ROLE_CUSTOMER_ADMIN);
            $this->findForEvaluator = $context->isGranted(Role::ROLE_EVALUATOR);
        } catch (NotFoundException $e) {
        } catch (NonAuthenticationUserException $e) {
        }
    }

    /**
     * @param Request $request
     */
    public function fillByRequest(Request $request)
    {
        $this->staffId = $request->get('staffId', $this->staffId);
        $this->mapStatuses($request);

        $this->fillPaginationByRequest($request);
    }

    /**
     * @param QueryBuilder $qb
     * @param string $alias
     */
    public function applyFilter(QueryBuilder $qb, $alias)
    {
        $expression = $this->getAccessComposite($qb, $alias);
        if ($expression) {
            $qb->andWhere($expression);
        }

        if ($this->staffId) {
            $qb->andwhere($alias . '.staff = :staffId')
                ->setParameter('staffId', $this->staffId);
        }

        if (count($this->statuses) > 0) {
            $qb->andwhere( $alias . '.status IN (:statuses)')
                ->setParameter('statuses', $this->statuses);
        }

        $this->applyPaginationFilter($qb, $alias);
    }

    /**
     * @param QueryBuilder $qb
     * @param string $alias
     * @return Composite
     */
    public function getAccessComposite(QueryBuilder $qb, $alias)
    {
        return $qb->expr()->orX(
            $this->findForOwner($qb, $alias),
            $this->findForEvaluatedStaff($qb, $alias),
            $this->findForEvaluator($qb, $alias),
            $this->findForAdmin($qb, $alias)
        );
    }

    /**
     * @param Request $request
     */
    private function mapStatuses(Request $request)
    {
        $stringStatuses = trim($request->get('status'));
        if (!$stringStatuses) {
            return;
        }
        $statuses = array_unique(explode(',', $stringStatuses));
        foreach ($statuses as $status) {
            $this->statuses[] = Status::getCodeByStatus($status);
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param string $alias
     * @return Comparison
     */
    private function findForOwner(QueryBuilder $qb, $alias)
    {
        return $this->currentUserId ? $qb->expr()->eq($alias . '.owner', $this->currentUserId) : null;
    }

    /**
     * @param QueryBuilder $qb
     * @param string $alias
     * @return Comparison
     */
    private function findForEvaluatedStaff(QueryBuilder $qb, $alias)
    {
        if (!$this->currentStaffId ) {
            return null;
        }

        return $qb->expr()->andX(
            $qb->expr()->eq($alias . '.staff', $this->currentStaffId),
            $qb->expr()->neq($alias . '.status', Status::_NEW_CODE),
            $qb->expr()->neq($alias . '.status', Status::CREATED_CODE)
        );
    }

    /**
     * @param QueryBuilder $qb
     * @param string $alias
     * @return Comparison
     */
    private function findForEvaluator(QueryBuilder $qb, $alias)
    {
        if (!$this->findForEvaluator) {
            return null;
        }

        return $qb->expr()->andX(
            $alias . '.staff IN (' . StaffRepository::getEvaluatedStaffQuery($this->currentStaffId, EvaluatedStaffTypes::PRIMARY_CODE) . ')',
            $qb->expr()->neq($alias . '.status', Status::_NEW_CODE)
        );
    }

    /**
     * @param QueryBuilder $qb
     * @param string $alias
     * @return Comparison
     */
    private function findForAdmin(QueryBuilder $qb, $alias)
    {
        if (!$this->findForAdmin ) {
            return null;
        }

        return $qb->expr()->andX(
            $qb->expr()->neq($alias . '.status', Status::_NEW_CODE)
        );
    }
}
