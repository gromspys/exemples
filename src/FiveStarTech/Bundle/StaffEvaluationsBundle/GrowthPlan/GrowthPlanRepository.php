<?php
/*
 * Copyright © Five-Star Technology Solutions
 *
 * For a full copyright notice, see the LICENSE file.
*/

namespace FiveStarTech\Bundle\StaffEvaluationsBundle\GrowthPlan;

use DateTime;
use Doctrine\ORM\EntityRepository;
use FiveStarTech\Bundle\CoreBundle\Status;
use FiveStarTech\Core\Model\HasSearch;
use FiveStarTech\Core\Model\ManageableInterface;
use FiveStarTech\Core\Model\ManageableTrait;
use FiveStarTech\Core\Model\PaginatorInterface;
use FiveStarTech\Core\Model\PaginatorTrait;
use FiveStarTech\Core\Model\CanRequest;
use FiveStarTech\Core\Model\RequestTrait;
use FiveStarTech\Core\Model\Search;
use FiveStarTech\Core\Model\SearchTrait;

/**
 * @author Sergey Korneev <sergey.korneev@opensoftdev.ru>
 */
class GrowthPlanRepository extends EntityRepository implements ManageableInterface, CanRequest, PaginatorInterface, HasSearch
{
    use ManageableTrait;
    use RequestTrait;
    use PaginatorTrait;
    use SearchTrait;

    /**
     * @return GrowthPlan[]
     */
    public function findByOverdue()
    {
        $nowDate = new DateTime();
        $overdueDate = $nowDate->modify('- 120 day');

        return $this->createQueryBuilder('g')
            ->where('g.endDate = :overdue_date')
            ->andWhere('g.status != :finalized')
            ->setParameter('overdue_date', $overdueDate)
            ->setParameter('finalized', Status::FINALIZED_CODE)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Search $search
     * @return bool
     */
    protected function supportSearch(Search $search)
    {
        return $search instanceof GrowthPlanSearch;
    }
}
