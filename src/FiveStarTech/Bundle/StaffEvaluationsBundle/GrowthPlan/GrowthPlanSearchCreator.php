<?php
/*
 * Copyright © Five-Star Technology Solutions
 *
 * For a full copyright notice, see the LICENSE file.
*/

namespace FiveStarTech\Bundle\StaffEvaluationsBundle\GrowthPlan;

use FiveStarTech\Bundle\CoreBundle\Search\Model\SearchCreator;
use FiveStarTech\Bundle\UserBundle\User\Handler\UserInvoker;
use FiveStarTech\Core\Model\Search;

/**
 * @author Sergey Korneev <sergey.korneev@opensoftdev.ru>
 */
class GrowthPlanSearchCreator implements SearchCreator
{
    /**
     * @var UserInvoker
     */
    private $userInvoker;

    /**
     * @param UserInvoker $userInvoker
     */
    public function __construct(UserInvoker $userInvoker)
    {
        $this->userInvoker = $userInvoker;
    }

    /**
     * @return Search
     */
    public function create()
    {
        return new GrowthPlanSearch($this->userInvoker);
    }
}