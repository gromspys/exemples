<?php
/*
 * Copyright © Five-Star Technology Solutions
 *
 * For a full copyright notice, see the LICENSE file.
*/

namespace FiveStarTech\Core\Model;

use Doctrine\ORM\QueryBuilder;
use FiveStarTech\Bundle\CoreBundle\Api\Exception\InvalidArgumentException;
use FiveStarTech\Bundle\CoreBundle\Search\Model\SearchCreator;

/**
 * @author Sergey Korneev <sergey.korneev@opensoftdev.ru>
 */
trait SearchTrait
{
    /**
     * @var SearchCreator
     */
    private $searchCreator;

    /**
     * @param string $alias
     * @return QueryBuilder
     */
    abstract protected function createQueryBuilder($alias);

    /**
     * @param Search $search
     * @return bool
     */
    abstract protected function supportSearch(Search $search);

    /**
     * @param SearchCreator $searchCreator
     */
    public function setSearchCreator(SearchCreator $searchCreator)
    {
        $this->searchCreator = $searchCreator;
    }

    /**
     * @return Search
     */
    public function createSearch()
    {
        return $this->searchCreator->create();
    }

    /**
     * @param Search $search
     * @return array
     * @throws InvalidArgumentException
     */
    public function search(Search $search)
    {
        if (!$this->supportSearch($search)) {
            throw new InvalidArgumentException(sprintf('Search "%s" don\'t supported', get_class($search)));
        }

        $qb = $this->createQueryBuilder('entity');

        $search->applyFilter($qb, 'entity');

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * @param Search $search
     * @return int
     * @throws InvalidArgumentException
     */
    public function count(Search $search)
    {
        if (!$this->supportSearch($search)) {
            throw new InvalidArgumentException(sprintf('Search "%s" don\'t supported', get_class($search)));
        }

        $qb = $this->createQueryBuilder('entity');

        $search->applyFilter($qb, 'entity');

        $qb->select('count(entity)');
        $qb->setFirstResult(null);
        $qb->setMaxResults(null);

        return $qb->getQuery()
            ->getSingleScalarResult();
    }
}
