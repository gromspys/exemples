<?php
/*
 * Copyright © Five-Star Technology Solutions
 *
 * For a full copyright notice, see the LICENSE file.
*/

namespace FiveStarTech\Core\Model;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Sergey Korneev <sergey.korneev@opensoftdev.ru>
 */
interface Search
{
    /**
     * @param Request $request
     */
    public function fillByRequest(Request $request);

    /**
     * @param QueryBuilder $qb
     * @param string $alias
     */
    public function applyFilter(QueryBuilder $qb, $alias);
}
